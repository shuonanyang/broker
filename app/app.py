from flask import Flask, request, render_template
import paho.mqtt.client as mqtt
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def homepage():
    
    # this block is only entered when the form is posted
    if request.method == 'POST':  
        # data from form input #G/00FF200025/011/0000
        topic = "G/"+request.form.get('serialNum')+"/" +request.form.get('msgType')+"/"+request.form.get('attId')
        #6aa07d00f002810280000100
        if request.form.get('onOff') is None:
            onOff = ''
        else:
            onOff = request.form.get('onOff')


	# Need to convert strint to hex for the payload
	
	
        payload =  request.form.get('deviceType')+request.form.get('deviceSerialNum')+ onOff
	payload = payload.replace(" ","")
	#print "before change to hex payload: ",payload
	payload_int = int(payload,16)
        payload = hex(payload_int)
	payload = "6aa07d00f002810280000100"

	#print "after change to hex payload: ",payload
	# connect to mqtt broker server
        connectClient(topic, payload)
#        on_message(client, userdata, msg)
        # make new inform page
        return '''<br><center><h1>Sending request to middleware... </h1><hr width="50%"><br><h2>topic: [{}]<br>payload: [{}]<h2></center>'''.format(topic,payload)
    # initial page form
    return render_template('home.html')
    
def connectClient(topic, payload):
    print "this is the hex payload",payload,"&&&"
    client = mqtt.Client()
#    client.on_message = on_message

    client.connect_async('127.0.0.1', 1883, 60)
    client.loop_start()
    client.publish(topic, payload=bytearray.fromhex(payload), qos=1)
    #client.publish(topic, payload, qos=1)
    #print(topic + ":" + payload)

if __name__ == '__main__':
    app.run(host ="0.0.0.0",port=5000,debug=True)
