# RULE OF SCRUM
2018 Summer Intern Project
## Goals
To build a proof of concept IoT system based on requirements gathered by our team in the first week.
#### Requirements

 - Build an IoT System
 - Unit testing
 - CI/CD
 - Test Automation
 - Web front end
 - Use Confluence and Jira

## Plan
A ten week, four sprint Scrum development cycle ending with a demo to ELT
