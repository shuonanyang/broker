var aedes = require('aedes')()
var server = require('net').createServer(aedes.handle)
var port = 1883

server.listen(port, () => 
{
	console.log('server listening on port', port);
});

server.on('client', (client) => 
{
	console.log('Client ' + client.id + ' connected');
});

server.on('clientDisconnect', (client) => 
{
	console.log('Client ' + client.id + ' disconnected');
});

server.on('publish', (packet, client) => 
{
	console.log(packet.payload.toString() + ' published to ' + packet.topic + ' by ' + client.id);
});

server.on('subscribe', (subscriptions, client) => 
{
	console.log('Client ' + client.id + ' subscribed to: ');
	for(var x of subscriptions)
	{
		console.log('\t' + x.topic + ' with QoS ' + x.qos);
	}		
});

server.on('unsubscribe', (unsubscriptions, client) =>
{
	console.log('Client ' + client.id + ' unsubscribed from: ');
	for(var x of unsubscriptions)
	{
		console.log('\t' + x);
	}
});
