var net = require("net")

const PORT = 1883;
//const PORT = 9883;

var server = net.createServer((socket) =>
{
    socket.on('data', (data) => 
    {
	console.log('DATA ' + socket.remoteAddress + ': ' + data);	
    });

    socket.on('close', (data) => {console.log('CLOSED');});

    socket.on('error', (err) => {console.log('ERROR ' + socket.remoteAddress + ': '
    + err.message);});
});

server.listen(PORT);
